#-------------------------------------------------
#
# Project created by QtCreator 2014-12-16T15:19:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = Atraktor
TEMPLATE = app


SOURCES += main.cpp\
    qcustomplot.cpp \
    atraktor.cpp

HEADERS  += \
    qcustomplot.h \
    atraktor.hpp

FORMS    += \
    atraktor.ui
