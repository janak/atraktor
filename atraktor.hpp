#ifndef ATRAKTOR_HPP
#define ATRAKTOR_HPP

#include <QWidget>
#include "qcustomplot.h"

namespace Ui {
class Atraktor;
}

class Atraktor : public QWidget
{
    Q_OBJECT

public:
    explicit Atraktor(QWidget *parent = 0);
    ~Atraktor();
    void reCalc();

private slots:
    void on_doubleSpinBox_valueChanged(double arg1);

    void on_horizontalSlider_sliderReleased();

    void on_horizontalSlider_valueChanged(int value);

    void on_pushButton_clicked();

private:
    Ui::Atraktor *ui;
    QCustomPlot *plot;
    double r;
    int N;
    QVector<double> n, x;
    bool animation;
};

#endif // ATRAKTOR_HPP
