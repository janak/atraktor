#include <QDebug>
#include <QThread>
#include "atraktor.hpp"
#include "ui_atraktor.h"

Atraktor::Atraktor(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Atraktor)
{
    ui->setupUi(this);
    r = ui->doubleSpinBox->value();
    ui->horizontalSlider->setValue((int) (r * 100));
    N = 1e2;
    plot = ui->widget;
    plot->addGraph();
    plot->xAxis->setLabel("n");
    plot->xAxis->setRange(0.0, 1.0);
    plot->yAxis->setLabel("x(n)");
    plot->yAxis->setRange(-1.0, 1.0);

    n = QVector<double>(N);
    for (int i = 0; i < N; ++i)
        n[i] = i/100.0;

    x = QVector<double>(N);
    x[0] = 1e-2;
    on_horizontalSlider_sliderReleased();

    animation = false;
}

Atraktor::~Atraktor()
{
    delete ui;
}

void Atraktor::on_doubleSpinBox_valueChanged(double arg1)
{
    r = arg1;
    ui->horizontalSlider->setValue((int) (arg1 * 100));
    on_horizontalSlider_sliderReleased();
}

void Atraktor::reCalc(void)
{
    ui->lineEdit->setText("Computing...");
    for (int i = 0; i < N-1; ++i) {
        x[i+1] = r * x[i] * (1.0 - x[i]);
        ui->progressBar->setValue(i+1);
    }
    ui->lineEdit->setText("Done!");
}

void Atraktor::on_horizontalSlider_valueChanged(int value)
{
    //ui->doubleSpinBox->setValue(value / 100.0);
    ui->horizontalSlider->setToolTipDuration(-1);
    ui->horizontalSlider->setToolTip(QString::number(value / 100.0));
}

void Atraktor::on_horizontalSlider_sliderReleased()
{
    int value = ui->horizontalSlider->value();
    ui->doubleSpinBox->setValue(value / 100.0);
    this->reCalc();
    plot->graph(0)->setData(n, x);
    plot->replot();
}


void Atraktor::on_pushButton_clicked()
{
    animation = !animation;
    ui->pushButton->setText("Stop");
    for (int p = 0; p < 1000; ++p) {
        if (!animation) break;
        r = (double) p / 100.0;
        on_horizontalSlider_sliderReleased();
        ui->horizontalSlider->setValue(p);
        QThread::msleep(300);
    }
    ui->pushButton->setText("Play");
}
